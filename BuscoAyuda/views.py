# coding=utf-8
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.core import serializers
from django.core.mail import EmailMessage
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.shortcuts import redirect, render, resolve_url
from django.template.response import TemplateResponse
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

from BuscoAyuda.forms import *
from BuscoAyuda.models import *


# ===========================================================================
# MAIN VIEWS
# ===========================================================================
def index(request):
    lista = Independiente.objects.all().order_by('-id')
    servicios_list = Servicio.objects.all().order_by('nombre')
    id_servicio = int(request.GET.get("idS", '0'))
    if id_servicio > 0:
        lista = Independiente.objects.filter(servicio__id=id_servicio).order_by('-id')
    paginator = Paginator(lista, 6)
    try:
        pagina = int(request.GET.get("page", '1'))
    except ValueError:
        pagina = 1
    try:
        lista = paginator.page(pagina)
    except (InvalidPage, EmptyPage):
        lista = paginator.page(paginator.num_pages)
    num_anterior = lista.number - 1
    num_siguiente = lista.number + 1
    comentario_form = ComentarioForm()
    data = {
        "independientes": lista,
        "servicios": servicios_list,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "comentario_form": comentario_form
    }
    return render(request, 'index.html', data)


def usuario(request):
    if "_auth_user_id" in request.session:
        uid = request.session["_auth_user_id"]
        user = User.objects.get(pk=uid)
        independiente = Independiente.objects.get(usuario=user)
        password_form = PasswordChangeForm(user=user)
        password_form.fields['old_password'].widget.attrs \
            .update({
            'placeholder': 'Contraseña antigua',
            'class': 'form-control'
        })
        password_form.fields['new_password1'].widget.attrs \
            .update({
            'placeholder': 'Contraseña nueva',
            'class': 'form-control'
        })
        password_form.fields['new_password2'].widget.attrs \
            .update({
            'placeholder': 'Contraseña nueva (confirmación)',
            'class': 'form-control'
        })
        if request.method == "POST":
            user_form = UserForm(request.POST, instance=user)
            form = IndependienteForm2(request.POST, request.FILES, instance=independiente)
            if form.is_valid() and user_form.is_valid():
                user_form.save()
                form.save()
                return redirect('index')
        else:
            user_form = UserForm(instance=user)
            form = IndependienteForm2(instance=independiente)
        data = {
            "form": form,
            "user_form": user_form,
            "password_form": password_form
        }
        return render(request, 'usuario.html', data)
    else:
        return redirect('index')


def comentario(request):
    if request.method == 'POST':
        form = ComentarioForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            id_independiente = cleaned_data.get('independiente_comentario')
            correo = cleaned_data.get('correo')
            contenido = cleaned_data.get('contenido')
            independiente_obj = Independiente.objects.get(id=id_independiente)
            Comentario.objects.create(correo=correo, contenido=contenido, independiente=independiente_obj)

            # CORREO
            mensaje = "Usted ha recibido un nuevo comentario en nuestro sistema:\n\n" \
                      "%s" \
                      "\n" \
                      "BuscoAyuda" % contenido
            try:
                headers = {'Reply-To': correo}
                email = EmailMessage("Nuevo Comentario", mensaje,
                                     'BuscoAyuda <misoagilesgrupo3@gmail.com>',
                                     [independiente_obj.usuario.email],
                                     [], headers=headers)
                email.send()
            except:
                print "Error al enviar el correo"
    return redirect('index')


def buscoayuda_add_independiente(request):
    messages = {}
    if request.method == 'POST':
        form = IndependienteForm(request.POST, request.FILES)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            username = cleaned_data.get('username')
            password = cleaned_data.get('password')
            first_name = cleaned_data.get('first_name')
            last_name = cleaned_data.get('last_name')
            email = cleaned_data.get('email')

            if User.objects.filter(username=username).count():
                messages['mensaje'] = "El usuario ya existe."
                return render(request, 'register.html', messages)
            else:
                user_model = User.objects.create_user(username=username, password=password)
                user_model.first_name = first_name
                user_model.last_name = last_name
                user_model.email = email
                user_model.save()

                experiencia = cleaned_data.get('experiencia')
                servicio = cleaned_data.get('servicio')
                telefono = cleaned_data.get('telefono')
                foto = cleaned_data.get('foto')
                Independiente.objects.create(experiencia=experiencia, telefono=telefono, foto=foto, servicio=servicio,
                                             usuario=user_model)
                return HttpResponseRedirect(reverse('index'))
    else:
        form = IndependienteForm()

    return render(request, 'register.html', {'form': form})


# ===========================================================================
# REST VIEWS
# ===========================================================================
def rest_comentarios(request, idIndependiente):
    data = {}
    if request.method == "GET":
        try:
            independiente_obj = Independiente.objects.get(id=idIndependiente)
            data = serializers.serialize("json",
                                         Comentario.objects.filter(independiente=independiente_obj).order_by('-id'))
        except:
            data = {}
    return HttpResponse(data, content_type='application/json; charset=UTF-8')


def rest_independiente(request, idIndependiente):
    data = {}
    if request.method == "GET":
        try:
            data = serializers.serialize("json", Independiente.objects.filter(id=idIndependiente))
        except:
            data = {}
    return HttpResponse(data, content_type='application/json; charset=UTF-8')


def rest_user(request, isUser):
    data = {}
    if request.method == "GET":
        try:
            data = serializers.serialize("json", User.objects.filter(id=isUser))
        except:
            data = {}
    return HttpResponse(data, content_type='application/json; charset=UTF-8')


def rest_servicio(request, idServicio):
    data = {}
    if request.method == "GET":
        try:
            data = serializers.serialize("json", Servicio.objects.filter(id=idServicio))
        except:
            data = {}
    return HttpResponse(data, content_type='application/json; charset=UTF-8')


# ===========================================================================
# OTHER VIEWS
# ===========================================================================
def buscoayuda_login(request):
    messages = {}
    # Está solicitando autenticación
    if request.method == "POST":
        username = request.POST['userid']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('index')
            else:
                messages['mensaje'] = "El usuario no está activo."
                # return render(request, 'index.html', messages)
                return redirect('index')
        else:
            messages['mensaje'] = "El usuario o contraseña que ha introducido no es correcto."
            # return render(request, 'index.html', messages)
            return redirect('index')
    # Es una peticion normal y ya existe una sesión previa
    else:
        if "_auth_user_id" in request.session:
            return redirect('index')
        else:
            # return render(request, 'index.html', messages)
            return redirect('index')


@login_required(login_url='/login')
def buscoayuda_logout(request):
    logout(request)
    return redirect('index')


@sensitive_post_parameters()
@csrf_protect
@login_required(login_url='/login')
def password_change(request,
                    template_name='registration/password_change_form.html',
                    post_change_redirect=None,
                    password_change_form=PasswordChangeForm,
                    extra_context=None):
    if post_change_redirect is None:
        post_change_redirect = reverse('password_change_done')
    else:
        post_change_redirect = resolve_url(post_change_redirect)
    if request.method == "POST":
        form = password_change_form(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            # Updating the password logs out all other sessions for the user
            # except the current one if
            # django.contrib.auth.middleware.SessionAuthenticationMiddleware
            # is enabled.
            update_session_auth_hash(request, form.user)
            return HttpResponseRedirect(post_change_redirect)
    else:
        form = password_change_form(user=request.user)
    context = {
        'form': form,
        'title': 'Password change',
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


@login_required(login_url='/login')
def password_change_done(request,
                         template_name='registration/password_change_done.html',
                         extra_context=None):
    context = {
        'title': 'Password change successful',
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)
