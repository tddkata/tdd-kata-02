__author__ = 'Cristian Huertas'

from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import random
import string


class FuncionalTest(TestCase):
    TestCase.textoAleatorio = ''.join(random.sample((string.ascii_uppercase + string.digits), 10))

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_1_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_2_registro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()

        wait = WebDriverWait(self.browser, 10)
        id_nombre = wait.until(EC.presence_of_element_located((By.ID, 'id_first_name')))

        nombre = self.browser.find_element_by_id('id_first_name')
        nombre.send_keys(self.textoAleatorio)

        apellido = self.browser.find_element_by_id('id_last_name')
        apellido.send_keys(self.textoAleatorio)

        experiencia = self.browser.find_element_by_id('id_experiencia')
        experiencia.send_keys('5')

        self.browser.find_element_by_xpath("//select[@id='id_servicio']/option[text()='Test1']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('6755443')

        correo = self.browser.find_element_by_id('id_email')
        correo.send_keys(self.textoAleatorio + '@uniandes.edu.co')

        imagen = self.browser.find_element_by_id('id_foto')
        imagen.send_keys('C:\Users\CristianCamilo\Desktop\desarrollador.jpg')

        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys(self.textoAleatorio)

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)

        span = self.browser.find_element(By.XPATH,
                                         '//span[text()="' + self.textoAleatorio + ' ' + self.textoAleatorio + '"]')
        self.assertIn(self.textoAleatorio + ' ' + self.textoAleatorio, span.text)

    def test_3_verDetalle(self):
        self.browser.get('http://localhost:8000')
        span = self.browser.find_element(By.XPATH,
                                         '//span[text()="' + self.textoAleatorio + ' ' + self.textoAleatorio + '"]')
        span.click()

        self.browser.implicitly_wait(3)

        h2 = self.browser.find_element(By.XPATH,
                                       '//h2[text()="' + self.textoAleatorio + ' ' + self.textoAleatorio + '"]')
        self.assertIn(self.textoAleatorio + ' ' + self.textoAleatorio, span.text, span.text)

    def test_4_login(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_login')
        link.click()

        wait = WebDriverWait(self.browser, 10)
        wait.until(EC.presence_of_element_located((By.ID, 'userid')))

        userid = self.browser.find_element_by_id('userid')
        userid.send_keys(self.textoAleatorio)

        password = self.browser.find_element_by_id('password')
        password.send_keys("clave123")

        btn_login = self.browser.find_element_by_id('btn_login')
        btn_login.click()

        wait = WebDriverWait(self.browser, 10)
        wait.until(EC.presence_of_element_located((By.ID, 'id_user')))

        id_user = self.browser.find_element_by_id('id_user')

        self.assertIn("Usuario", id_user.text)

    def test_5_edicion(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_login')
        link.click()

        wait = WebDriverWait(self.browser, 10)
        wait.until(EC.presence_of_element_located((By.ID, 'userid')))

        userid = self.browser.find_element_by_id('userid')
        userid.send_keys(self.textoAleatorio)

        password = self.browser.find_element_by_id('password')
        password.send_keys("clave123")

        btn_login = self.browser.find_element_by_id('btn_login')
        btn_login.click()

        wait = WebDriverWait(self.browser, 10)
        wait.until(EC.presence_of_element_located((By.ID, 'id_user')))

        btn_login = self.browser.find_element_by_id('id_user')
        btn_login.click()

        wait = WebDriverWait(self.browser, 10)
        id_nombre = wait.until(EC.presence_of_element_located((By.ID, 'id_first_name')))

        nombre = self.browser.find_element_by_id('id_first_name')
        nombre.send_keys("MOD")

        apellido = self.browser.find_element_by_id('id_last_name')
        apellido.send_keys("MOD")

        experiencia = self.browser.find_element_by_id('id_experiencia')
        experiencia.send_keys('0')

        correo = self.browser.find_element_by_id('id_email')
        correo.send_keys("MOD")

        botonGrabar = self.browser.find_element_by_id('id_actualizar')
        botonGrabar.click()

        wait = WebDriverWait(self.browser, 10)
        wait.until(EC.presence_of_element_located((By.ID, 'id_user')))

        span = self.browser.find_element(By.XPATH,
                                         '//span[text()="' + self.textoAleatorio + 'MOD ' + self.textoAleatorio + 'MOD"]')
        self.assertIn(self.textoAleatorio + 'MOD ' + self.textoAleatorio + 'MOD', span.text)

    def test_6_comentario(self):
        self.browser.get('http://localhost:8000')
        self.browser.find_element(By.XPATH,
                                  '//span[text()="' + self.textoAleatorio + 'MOD ' + self.textoAleatorio + 'MOD"]').click()

        self.browser.implicitly_wait(3)

        self.browser.find_element_by_id('id_agregar').click()

        id_contenido = self.browser.find_element_by_id('id_contenido')
        id_contenido.send_keys(self.textoAleatorio)

        id_correo = self.browser.find_element_by_id('id_correo')
        id_correo.send_keys(self.textoAleatorio + '@uniandes.edu.co')

        self.browser.find_element_by_id('id_enviar').click()

        self.browser.implicitly_wait(3)

        self.browser.find_element(By.XPATH,
                                  '//span[text()="' + self.textoAleatorio + 'MOD ' + self.textoAleatorio + 'MOD"]').click()

        id_comentario = self.browser.find_element_by_id('id_comentario')

        self.assertIn(self.textoAleatorio, id_comentario.text)


    def test_7_registro(self):
        self.browser.get('http://localhost:8000')

        self.browser.find_element_by_xpath("//select[@id='slctBusquedaServicio']/option[text()='Test1']").click()

        self.browser.implicitly_wait(6)

        span = self.browser.find_element(By.XPATH,
                                         '//span[text()="' + self.textoAleatorio + 'MOD ' + self.textoAleatorio + 'MOD"]')
        self.assertIn(self.textoAleatorio + 'MOD ' + self.textoAleatorio+'MOD', span.text)